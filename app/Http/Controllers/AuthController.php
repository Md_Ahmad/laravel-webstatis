<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function daftar(){
        return view('halaman.form');
    }

    
    public function welcome(Request $request){
        // dd($request->all());

        $firstname = $request['first'];
        $lastname = $request['last'];

        return view('halaman.welcome', compact('firstname','lastname'));
    }
}