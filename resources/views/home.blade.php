<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Halama Utama | M Ahmad</title>
  </head>
  <body>
    <div class="container" style="position: absolute; top: 30%; left: 50%; transform: translate(-50%, -50%);">
      <h1 style="color: rgb(250, 100, 83)">Sanber Media Online</h1>
      <h3>Sosial Media Developer</h3>
      <p>Belajar dan Berbagi agar hidup lebih baik</p>
      <h3>Benefit Join di Media Online</h3>
      <ul>
        <li>Mendapatkan Motivasi dari sesama para Developer</li>
        <li>Sharing Knowledge</li>
        <li>Dibuat oleh calon Web developer terbaik</li>
      </ul>
      <h3>Cara Bergabung ke Media Online</h3>
      <ol>
        <li>Mengunjungi Website ini</li>
        <li>Mendaftarkan di <a href="/register" style="font-weight: bold; color:blue" title="Silahkan klik untuk mendaftar.."> Form Sign Up</a></li>
        <li>Selesai</li>
      </ol>
    </div>
  </body>
</html>